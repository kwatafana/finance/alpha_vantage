//! 🚨 error management
use std::fmt;

#[derive(Debug)]
/// 🚨 error management
pub enum Error {
    /// Network error
    Network,
    /// Data format error
    DataFormat,
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Network => write!(f, "Networking error"),
            Self::DataFormat => write!(f, "Data format error"),
        }
    }
}
