pub use error::Error;
use kwatadata::IncomeStatementInput;
use serde::{Deserialize, Serialize};
const HOST: &str = "https://www.alphavantage.co/";

mod error;

pub struct Client {
    pub key: String,
}

impl Client {
    // Get income statements
    pub fn income_statement(&self, symbol: &str) -> Result<Vec<IncomeStatementInput>, Error> {
        let key = &self.key;
        let url = format!("{HOST}query?function=INCOME_STATEMENT&symbol={symbol}&apikey={key}");
        let res = reqwest::blocking::get(url);

        if let Ok(res) = res {
            if !res.status().is_success() {
                return Err(Error::Network);
            }

            let income_statements: IncomeStatementResponse =
                res.json().map_err(|_| Error::DataFormat)?;

            Ok(income_statements.annual_reports)
        } else {
            return Err(Error::Network);
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct IncomeStatementResponse {
    symbol: Option<String>,
    annual_reports: Vec<IncomeStatementInput>,
}
